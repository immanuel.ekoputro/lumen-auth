<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
$router->options(
    '/{any:.*}',
    [
        function () {
            return response(['status' => 'success']);
        }
    ]
);

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->group([
    'as' => 'api',
    'prefix' => 'v1'
], function () use ($router) {
    /**
     * User Endpoints
     */
    $router->group([
        'as' => 'user',
        'prefix' => 'user'
    ], function () use ($router) {
        $router->group([], function () use ($router) {
            $router->post('/', 'UserController@fetchDetailUser');
        });
    });
});
