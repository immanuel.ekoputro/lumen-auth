<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function fetchDetailUser(Request $request)
    {
        $user = Auth::user();
        return response(['user' => ($user) ? $user : []]);
    }
}
